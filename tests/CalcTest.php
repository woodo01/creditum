<?php

namespace App\Tests\Utils;

use App\Utils\Calculator;
use PHPUnit\Framework\TestCase;

class CalcTest extends TestCase
{
    /** @var Calculator */
    private $calc;

    protected function setUp()
    {
        parent::setUp();
        $this->calc = new Calculator();
    }

    /**
     * @dataProvider getData
     */
    public function testCalc($credit, $rate, $time)
    {
        $table = $this->calc->calc($credit, $rate, $time)['table'];
        $creditSum = $procSum = 0;
        foreach ($table as $row) {
            $creditSum += $row['creditSum'];
            $procSum += $row['procSum'];
        }

        $this->assertEquals($procSum, $rate * $time / 12 / 100 * $credit);
        $this->assertEquals($creditSum, $credit);
    }

    public function getData()
    {
        yield [100000, 10, 1];
        yield [100000, 10, 2];
        yield [100000, 10, 3];
        yield [100000, 10, 4];
        yield [100000, 10, 5];
        yield [100000, 10, 6];
        yield [100000, 10, 7];
        yield [100000, 10, 8];
        yield [100000, 10, 9];
        yield [100000, 10, 10];
        yield [100000, 10, 11];
        yield [100000, 10, 12];
    }
}
