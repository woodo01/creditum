<?php
/**
 * Created by IntelliJ IDEA.
 * User: ivansigaev
 * Date: 2019-05-30
 * Time: 18:15
 */

namespace App\Form;


use App\Entity\CardType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CardtypeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
            ])
            ->add('attributes', EntityType::class, [
                'class' => \App\Entity\Attribute::class,
                'choice_label' => 'name',
                'label' => 'label.attribute_name',
                'help' => 'help.attribute_name_publication',
                'multiple' => true,
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CardType::class,
        ]);
    }
}