<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Entity\PromoText;
use App\Entity\User;
use App\Repository\PromoTextRepository;
use App\Repository\UserRepository;
use App\Utils\Slugger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * A console command that lists all the existing users.
 *
 * To use this command, open a terminal window, enter into your project directory
 * and execute the following:
 *
 *     $ php bin/console app:list-users
 *
 * See https://symfony.com/doc/current/console.html
 * For more advanced uses, commands can be defined as services too. See
 * https://symfony.com/doc/current/console/commands_as_services.html
 *
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class SyncCommand extends Command
{
    // a good practice is to use the 'app:' prefix to group all your custom application commands
    protected static $defaultName = 'app:sync';
    private $texts;
    private $entityManager;

    public function __construct(EntityManagerInterface $em, PromoTextRepository $repo)
    {
        parent::__construct();

        $this->texts = $repo;
        $this->entityManager = $em;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Syncs');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = file_get_contents(__DIR__ . '/1.html');
        $arr = explode('-----', $file);

        $result = [];
        foreach ($arr as $a) {
            if (preg_match('/.*\[\d+\]/', $a, $h1)) {
                $result[] = [
                    'h1' => $h1,
                    'text' => str_replace($h1, '', $a)
                ];

                $promoText = new PromoText();
                $promoText->setContent(str_replace($h1[0], '', $a));
                $promoText->setTag(preg_replace('/\s+\[\d+\]$/', '', $h1[0]));
                $promoText->setTitle($h1[0]);
                $promoText->setSlug(Slugger::slugify($h1[0]));
                $this->entityManager->persist($promoText);
                $this->entityManager->flush();
            }
        }

        var_dump($result);
    }
}
