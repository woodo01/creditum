<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Repository\CardRepository;
use App\Utils\Slugger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TranslitCardsCommand extends Command
{
    protected static $defaultName = 'app:translit-cards';

    private $cards;
    private $entityManager;

    public function __construct(CardRepository $cards, EntityManagerInterface $em)
    {
        parent::__construct();

        $this->cards = $cards;
        $this->entityManager = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $cards = $this->cards->findBy([]);
        foreach ($cards as $card) {
            $card->setSlug(Slugger::slugify($card->getSlug()));
            $this->entityManager->persist($card);
        }

        $this->entityManager->flush();
    }
}
