<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use App\Repository\PromoTextRepository;
use App\Utils\Slugger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixSlugCommand extends Command
{
    protected static $defaultName = 'app:fix-slug';

    private $promoTextRepository;
    private $entityManager;

    public function __construct(PromoTextRepository $promoTextRepository, EntityManagerInterface $em)
    {
        parent::__construct();

        $this->promoTextRepository = $promoTextRepository;
        $this->entityManager = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $pages = $this->promoTextRepository->findBy([]);

        foreach ($pages as $page) {
            $slug = $page->getSlug();
            $slug = preg_replace('/\-\[[\d]+\]$/', '', $slug);
            $page->setSlug($slug);

            $title = $page->getTitle();
            $title = preg_replace('/\s\[[\d]+\]$/', '', $title);
            $page->setTitle($title);

            $this->entityManager->persist($page);
        }

        $this->entityManager->flush();
    }
}
