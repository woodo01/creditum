<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190915222444 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_161498D3989D9B62');
        $this->addSql('DROP INDEX IDX_161498D3C54C8C93');
        $this->addSql('CREATE TEMPORARY TABLE __temp__card AS SELECT id, type_id, description, content, image, short_description, title, slug, credit_limit_from, credit_limit_to, credit_limit_formatted, grace_period_from, grace_period_to, grace_period_formatted, annual_rate_from, annual_rate_to, annual_rate_formatted, service_cost_from, service_cost_to, service_cost_formatted, cashback_formatted, cashback_from, cashback_to, interest_on_balance_to, interest_on_balance_formatted, interest_on_balance_from, issue_period_formatted, issue_period_from, issue_period_to FROM card');
        $this->addSql('DROP TABLE card');
        $this->addSql('CREATE TABLE card (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type_id INTEGER NOT NULL, description VARCHAR(255) DEFAULT NULL COLLATE BINARY, content CLOB DEFAULT NULL COLLATE BINARY, image VARCHAR(255) DEFAULT NULL COLLATE BINARY, short_description VARCHAR(255) DEFAULT NULL COLLATE BINARY, title VARCHAR(255) NOT NULL COLLATE BINARY, slug VARCHAR(255) NOT NULL COLLATE BINARY, credit_limit_from DOUBLE PRECISION DEFAULT NULL, credit_limit_to DOUBLE PRECISION DEFAULT NULL, credit_limit_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, grace_period_from DOUBLE PRECISION DEFAULT NULL, grace_period_to DOUBLE PRECISION DEFAULT NULL, grace_period_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, annual_rate_from DOUBLE PRECISION DEFAULT NULL, annual_rate_to DOUBLE PRECISION DEFAULT NULL, annual_rate_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, service_cost_from DOUBLE PRECISION DEFAULT NULL, service_cost_to DOUBLE PRECISION DEFAULT NULL, service_cost_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, cashback_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, cashback_from DOUBLE PRECISION DEFAULT NULL, cashback_to DOUBLE PRECISION DEFAULT NULL, interest_on_balance_to DOUBLE PRECISION DEFAULT NULL, interest_on_balance_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, interest_on_balance_from DOUBLE PRECISION DEFAULT NULL, issue_period_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, issue_period_from DOUBLE PRECISION DEFAULT NULL, issue_period_to DOUBLE PRECISION DEFAULT NULL, CONSTRAINT FK_161498D3C54C8C93 FOREIGN KEY (type_id) REFERENCES card_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO card (id, type_id, description, content, image, short_description, title, slug, credit_limit_from, credit_limit_to, credit_limit_formatted, grace_period_from, grace_period_to, grace_period_formatted, annual_rate_from, annual_rate_to, annual_rate_formatted, service_cost_from, service_cost_to, service_cost_formatted, cashback_formatted, cashback_from, cashback_to, interest_on_balance_to, interest_on_balance_formatted, interest_on_balance_from, issue_period_formatted, issue_period_from, issue_period_to) SELECT id, type_id, description, content, image, short_description, title, slug, credit_limit_from, credit_limit_to, credit_limit_formatted, grace_period_from, grace_period_to, grace_period_formatted, annual_rate_from, annual_rate_to, annual_rate_formatted, service_cost_from, service_cost_to, service_cost_formatted, cashback_formatted, cashback_from, cashback_to, interest_on_balance_to, interest_on_balance_formatted, interest_on_balance_from, issue_period_formatted, issue_period_from, issue_period_to FROM __temp__card');
        $this->addSql('DROP TABLE __temp__card');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_161498D3989D9B62 ON card (slug)');
        $this->addSql('CREATE INDEX IDX_161498D3C54C8C93 ON card (type_id)');
        $this->addSql('DROP INDEX IDX_24FB05CE4ACC9A20');
        $this->addSql('DROP INDEX IDX_24FB05CEC44FBE02');
        $this->addSql('CREATE TEMPORARY TABLE __temp__card_filter_value AS SELECT card_id, filter_value_id FROM card_filter_value');
        $this->addSql('DROP TABLE card_filter_value');
        $this->addSql('CREATE TABLE card_filter_value (card_id INTEGER NOT NULL, filter_value_id INTEGER NOT NULL, PRIMARY KEY(card_id, filter_value_id), CONSTRAINT FK_24FB05CE4ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_24FB05CEC44FBE02 FOREIGN KEY (filter_value_id) REFERENCES filter_value (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO card_filter_value (card_id, filter_value_id) SELECT card_id, filter_value_id FROM __temp__card_filter_value');
        $this->addSql('DROP TABLE __temp__card_filter_value');
        $this->addSql('CREATE INDEX IDX_24FB05CE4ACC9A20 ON card_filter_value (card_id)');
        $this->addSql('CREATE INDEX IDX_24FB05CEC44FBE02 ON card_filter_value (filter_value_id)');
        $this->addSql('DROP INDEX IDX_FE4FBB82B6E62EFA');
        $this->addSql('DROP INDEX IDX_FE4FBB824ACC9A20');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_value AS SELECT id, card_id, attribute_id, value, value_formatted FROM attribute_value');
        $this->addSql('DROP TABLE attribute_value');
        $this->addSql('CREATE TABLE attribute_value (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, card_id INTEGER NOT NULL, attribute_id INTEGER NOT NULL, value VARCHAR(255) NOT NULL COLLATE BINARY, value_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_FE4FBB824ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_FE4FBB82B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO attribute_value (id, card_id, attribute_id, value, value_formatted) SELECT id, card_id, attribute_id, value, value_formatted FROM __temp__attribute_value');
        $this->addSql('DROP TABLE __temp__attribute_value');
        $this->addSql('CREATE INDEX IDX_FE4FBB82B6E62EFA ON attribute_value (attribute_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB824ACC9A20 ON attribute_value (card_id)');
        $this->addSql('DROP INDEX IDX_DDCC250D925606E5');
        $this->addSql('DROP INDEX IDX_DDCC250DB6E62EFA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_card_type AS SELECT attribute_id, card_type_id FROM attribute_card_type');
        $this->addSql('DROP TABLE attribute_card_type');
        $this->addSql('CREATE TABLE attribute_card_type (attribute_id INTEGER NOT NULL, card_type_id INTEGER NOT NULL, PRIMARY KEY(attribute_id, card_type_id), CONSTRAINT FK_DDCC250DB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_DDCC250D925606E5 FOREIGN KEY (card_type_id) REFERENCES card_type (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO attribute_card_type (attribute_id, card_type_id) SELECT attribute_id, card_type_id FROM __temp__attribute_card_type');
        $this->addSql('DROP TABLE __temp__attribute_card_type');
        $this->addSql('CREATE INDEX IDX_DDCC250D925606E5 ON attribute_card_type (card_type_id)');
        $this->addSql('CREATE INDEX IDX_DDCC250DB6E62EFA ON attribute_card_type (attribute_id)');
        $this->addSql('DROP INDEX IDX_711204C161F98D7A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__promo_text AS SELECT id, promo_text_id, tag, h1, seo_title, description, slug, title, content FROM promo_text');
        $this->addSql('DROP TABLE promo_text');
        $this->addSql('CREATE TABLE promo_text (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, promo_text_id INTEGER DEFAULT NULL, tag VARCHAR(255) DEFAULT NULL COLLATE BINARY, h1 VARCHAR(255) DEFAULT NULL COLLATE BINARY, seo_title VARCHAR(255) DEFAULT NULL COLLATE BINARY, description VARCHAR(255) DEFAULT NULL COLLATE BINARY, slug VARCHAR(255) NOT NULL COLLATE BINARY, title VARCHAR(255) NOT NULL COLLATE BINARY, content CLOB NOT NULL COLLATE BINARY, CONSTRAINT FK_711204C161F98D7A FOREIGN KEY (promo_text_id) REFERENCES promo_text (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO promo_text (id, promo_text_id, tag, h1, seo_title, description, slug, title, content) SELECT id, promo_text_id, tag, h1, seo_title, description, slug, title, content FROM __temp__promo_text');
        $this->addSql('DROP TABLE __temp__promo_text');
        $this->addSql('CREATE INDEX IDX_711204C161F98D7A ON promo_text (promo_text_id)');
        $this->addSql('DROP INDEX IDX_42FB0C88C44FBE02');
        $this->addSql('DROP INDEX IDX_42FB0C8861F98D7A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__promo_text_filter_value AS SELECT promo_text_id, filter_value_id FROM promo_text_filter_value');
        $this->addSql('DROP TABLE promo_text_filter_value');
        $this->addSql('CREATE TABLE promo_text_filter_value (promo_text_id INTEGER NOT NULL, filter_value_id INTEGER NOT NULL, PRIMARY KEY(promo_text_id, filter_value_id), CONSTRAINT FK_42FB0C8861F98D7A FOREIGN KEY (promo_text_id) REFERENCES promo_text (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_42FB0C88C44FBE02 FOREIGN KEY (filter_value_id) REFERENCES filter_value (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO promo_text_filter_value (promo_text_id, filter_value_id) SELECT promo_text_id, filter_value_id FROM __temp__promo_text_filter_value');
        $this->addSql('DROP TABLE __temp__promo_text_filter_value');
        $this->addSql('CREATE INDEX IDX_42FB0C88C44FBE02 ON promo_text_filter_value (filter_value_id)');
        $this->addSql('CREATE INDEX IDX_42FB0C8861F98D7A ON promo_text_filter_value (promo_text_id)');
        $this->addSql('DROP INDEX IDX_34C6ABCBD395B25E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__filter_value AS SELECT id, filter_id, value FROM filter_value');
        $this->addSql('DROP TABLE filter_value');
        $this->addSql('CREATE TABLE filter_value (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, filter_id INTEGER NOT NULL, value VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_34C6ABCBD395B25E FOREIGN KEY (filter_id) REFERENCES filter (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO filter_value (id, filter_id, value) SELECT id, filter_id, value FROM __temp__filter_value');
        $this->addSql('DROP TABLE __temp__filter_value');
        $this->addSql('CREATE INDEX IDX_34C6ABCBD395B25E ON filter_value (filter_id)');
        $this->addSql('DROP INDEX IDX_7FC45F1D925606E5');
        $this->addSql('CREATE TEMPORARY TABLE __temp__filter AS SELECT id, card_type_id, name, slug FROM filter');
        $this->addSql('DROP TABLE filter');
        $this->addSql('CREATE TABLE filter (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, card_type_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, slug VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_7FC45F1D925606E5 FOREIGN KEY (card_type_id) REFERENCES card_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO filter (id, card_type_id, name, slug) SELECT id, card_type_id, name, slug FROM __temp__filter');
        $this->addSql('DROP TABLE __temp__filter');
        $this->addSql('CREATE INDEX IDX_7FC45F1D925606E5 ON filter (card_type_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_DDCC250DB6E62EFA');
        $this->addSql('DROP INDEX IDX_DDCC250D925606E5');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_card_type AS SELECT attribute_id, card_type_id FROM attribute_card_type');
        $this->addSql('DROP TABLE attribute_card_type');
        $this->addSql('CREATE TABLE attribute_card_type (attribute_id INTEGER NOT NULL, card_type_id INTEGER NOT NULL, PRIMARY KEY(attribute_id, card_type_id))');
        $this->addSql('INSERT INTO attribute_card_type (attribute_id, card_type_id) SELECT attribute_id, card_type_id FROM __temp__attribute_card_type');
        $this->addSql('DROP TABLE __temp__attribute_card_type');
        $this->addSql('CREATE INDEX IDX_DDCC250DB6E62EFA ON attribute_card_type (attribute_id)');
        $this->addSql('CREATE INDEX IDX_DDCC250D925606E5 ON attribute_card_type (card_type_id)');
        $this->addSql('DROP INDEX IDX_FE4FBB824ACC9A20');
        $this->addSql('DROP INDEX IDX_FE4FBB82B6E62EFA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_value AS SELECT id, card_id, attribute_id, value, value_formatted FROM attribute_value');
        $this->addSql('DROP TABLE attribute_value');
        $this->addSql('CREATE TABLE attribute_value (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, card_id INTEGER NOT NULL, attribute_id INTEGER NOT NULL, value VARCHAR(255) NOT NULL, value_formatted VARCHAR(255) DEFAULT NULL)');
        $this->addSql('INSERT INTO attribute_value (id, card_id, attribute_id, value, value_formatted) SELECT id, card_id, attribute_id, value, value_formatted FROM __temp__attribute_value');
        $this->addSql('DROP TABLE __temp__attribute_value');
        $this->addSql('CREATE INDEX IDX_FE4FBB824ACC9A20 ON attribute_value (card_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82B6E62EFA ON attribute_value (attribute_id)');
        $this->addSql('DROP INDEX UNIQ_161498D3989D9B62');
        $this->addSql('DROP INDEX IDX_161498D3C54C8C93');
        $this->addSql('CREATE TEMPORARY TABLE __temp__card AS SELECT id, type_id, title, image, description, slug, content, short_description, credit_limit_from, credit_limit_to, credit_limit_formatted, issue_period_from, issue_period_to, issue_period_formatted, cashback_from, cashback_to, cashback_formatted, grace_period_from, grace_period_to, grace_period_formatted, annual_rate_from, annual_rate_to, annual_rate_formatted, service_cost_from, service_cost_to, service_cost_formatted, interest_on_balance_from, interest_on_balance_to, interest_on_balance_formatted FROM card');
        $this->addSql('DROP TABLE card');
        $this->addSql('CREATE TABLE card (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type_id INTEGER NOT NULL, title VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, content CLOB DEFAULT NULL, short_description VARCHAR(255) DEFAULT NULL, credit_limit_from DOUBLE PRECISION DEFAULT NULL, credit_limit_to DOUBLE PRECISION DEFAULT NULL, credit_limit_formatted VARCHAR(255) DEFAULT NULL, issue_period_from DOUBLE PRECISION DEFAULT NULL, issue_period_to DOUBLE PRECISION DEFAULT NULL, issue_period_formatted VARCHAR(255) DEFAULT NULL, cashback_from DOUBLE PRECISION DEFAULT NULL, cashback_to DOUBLE PRECISION DEFAULT NULL, cashback_formatted VARCHAR(255) DEFAULT NULL, grace_period_from DOUBLE PRECISION DEFAULT NULL, grace_period_to DOUBLE PRECISION DEFAULT NULL, grace_period_formatted VARCHAR(255) DEFAULT NULL, annual_rate_from DOUBLE PRECISION DEFAULT NULL, annual_rate_to DOUBLE PRECISION DEFAULT NULL, annual_rate_formatted VARCHAR(255) DEFAULT NULL, service_cost_from DOUBLE PRECISION DEFAULT NULL, service_cost_to DOUBLE PRECISION DEFAULT NULL, service_cost_formatted VARCHAR(255) DEFAULT NULL, interest_on_balance_from DOUBLE PRECISION DEFAULT NULL, interest_on_balance_to DOUBLE PRECISION DEFAULT NULL, interest_on_balance_formatted VARCHAR(255) DEFAULT NULL)');
        $this->addSql('INSERT INTO card (id, type_id, title, image, description, slug, content, short_description, credit_limit_from, credit_limit_to, credit_limit_formatted, issue_period_from, issue_period_to, issue_period_formatted, cashback_from, cashback_to, cashback_formatted, grace_period_from, grace_period_to, grace_period_formatted, annual_rate_from, annual_rate_to, annual_rate_formatted, service_cost_from, service_cost_to, service_cost_formatted, interest_on_balance_from, interest_on_balance_to, interest_on_balance_formatted) SELECT id, type_id, title, image, description, slug, content, short_description, credit_limit_from, credit_limit_to, credit_limit_formatted, issue_period_from, issue_period_to, issue_period_formatted, cashback_from, cashback_to, cashback_formatted, grace_period_from, grace_period_to, grace_period_formatted, annual_rate_from, annual_rate_to, annual_rate_formatted, service_cost_from, service_cost_to, service_cost_formatted, interest_on_balance_from, interest_on_balance_to, interest_on_balance_formatted FROM __temp__card');
        $this->addSql('DROP TABLE __temp__card');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_161498D3989D9B62 ON card (slug)');
        $this->addSql('CREATE INDEX IDX_161498D3C54C8C93 ON card (type_id)');
        $this->addSql('DROP INDEX IDX_24FB05CE4ACC9A20');
        $this->addSql('DROP INDEX IDX_24FB05CEC44FBE02');
        $this->addSql('CREATE TEMPORARY TABLE __temp__card_filter_value AS SELECT card_id, filter_value_id FROM card_filter_value');
        $this->addSql('DROP TABLE card_filter_value');
        $this->addSql('CREATE TABLE card_filter_value (card_id INTEGER NOT NULL, filter_value_id INTEGER NOT NULL, PRIMARY KEY(card_id, filter_value_id))');
        $this->addSql('INSERT INTO card_filter_value (card_id, filter_value_id) SELECT card_id, filter_value_id FROM __temp__card_filter_value');
        $this->addSql('DROP TABLE __temp__card_filter_value');
        $this->addSql('CREATE INDEX IDX_24FB05CE4ACC9A20 ON card_filter_value (card_id)');
        $this->addSql('CREATE INDEX IDX_24FB05CEC44FBE02 ON card_filter_value (filter_value_id)');
        $this->addSql('DROP INDEX IDX_7FC45F1D925606E5');
        $this->addSql('CREATE TEMPORARY TABLE __temp__filter AS SELECT id, card_type_id, name, slug FROM filter');
        $this->addSql('DROP TABLE filter');
        $this->addSql('CREATE TABLE filter (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, card_type_id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO filter (id, card_type_id, name, slug) SELECT id, card_type_id, name, slug FROM __temp__filter');
        $this->addSql('DROP TABLE __temp__filter');
        $this->addSql('CREATE INDEX IDX_7FC45F1D925606E5 ON filter (card_type_id)');
        $this->addSql('DROP INDEX IDX_34C6ABCBD395B25E');
        $this->addSql('CREATE TEMPORARY TABLE __temp__filter_value AS SELECT id, filter_id, value FROM filter_value');
        $this->addSql('DROP TABLE filter_value');
        $this->addSql('CREATE TABLE filter_value (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, filter_id INTEGER NOT NULL, value VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO filter_value (id, filter_id, value) SELECT id, filter_id, value FROM __temp__filter_value');
        $this->addSql('DROP TABLE __temp__filter_value');
        $this->addSql('CREATE INDEX IDX_34C6ABCBD395B25E ON filter_value (filter_id)');
        $this->addSql('DROP INDEX IDX_711204C161F98D7A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__promo_text AS SELECT id, promo_text_id, slug, title, content, tag, h1, seo_title, description FROM promo_text');
        $this->addSql('DROP TABLE promo_text');
        $this->addSql('CREATE TABLE promo_text (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, promo_text_id INTEGER DEFAULT NULL, slug VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, content CLOB NOT NULL, tag VARCHAR(255) DEFAULT NULL, h1 VARCHAR(255) DEFAULT NULL, seo_title VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL)');
        $this->addSql('INSERT INTO promo_text (id, promo_text_id, slug, title, content, tag, h1, seo_title, description) SELECT id, promo_text_id, slug, title, content, tag, h1, seo_title, description FROM __temp__promo_text');
        $this->addSql('DROP TABLE __temp__promo_text');
        $this->addSql('CREATE INDEX IDX_711204C161F98D7A ON promo_text (promo_text_id)');
        $this->addSql('DROP INDEX IDX_42FB0C8861F98D7A');
        $this->addSql('DROP INDEX IDX_42FB0C88C44FBE02');
        $this->addSql('CREATE TEMPORARY TABLE __temp__promo_text_filter_value AS SELECT promo_text_id, filter_value_id FROM promo_text_filter_value');
        $this->addSql('DROP TABLE promo_text_filter_value');
        $this->addSql('CREATE TABLE promo_text_filter_value (promo_text_id INTEGER NOT NULL, filter_value_id INTEGER NOT NULL, PRIMARY KEY(promo_text_id, filter_value_id))');
        $this->addSql('INSERT INTO promo_text_filter_value (promo_text_id, filter_value_id) SELECT promo_text_id, filter_value_id FROM __temp__promo_text_filter_value');
        $this->addSql('DROP TABLE __temp__promo_text_filter_value');
        $this->addSql('CREATE INDEX IDX_42FB0C8861F98D7A ON promo_text_filter_value (promo_text_id)');
        $this->addSql('CREATE INDEX IDX_42FB0C88C44FBE02 ON promo_text_filter_value (filter_value_id)');
    }
}
