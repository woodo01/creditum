<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190813201637 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_161498D3C54C8C93');
        $this->addSql('CREATE TEMPORARY TABLE __temp__card AS SELECT id, type_id, title, description, slug, content, image, short_description FROM card');
        $this->addSql('DROP TABLE card');
        $this->addSql('CREATE TABLE card (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type_id INTEGER NOT NULL, title VARCHAR(255) NOT NULL COLLATE BINARY, description VARCHAR(255) DEFAULT NULL COLLATE BINARY, slug VARCHAR(255) NOT NULL COLLATE BINARY, content CLOB DEFAULT NULL COLLATE BINARY, image VARCHAR(255) DEFAULT NULL COLLATE BINARY, short_description VARCHAR(255) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_161498D3C54C8C93 FOREIGN KEY (type_id) REFERENCES card_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO card (id, type_id, title, description, slug, content, image, short_description) SELECT id, type_id, title, description, slug, content, image, short_description FROM __temp__card');
        $this->addSql('DROP TABLE __temp__card');
        $this->addSql('CREATE INDEX IDX_161498D3C54C8C93 ON card (type_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_161498D3989D9B62 ON card (slug)');
        $this->addSql('DROP INDEX IDX_FE4FBB824ACC9A20');
        $this->addSql('DROP INDEX IDX_FE4FBB82B6E62EFA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_value AS SELECT id, card_id, attribute_id, value, value_formatted FROM attribute_value');
        $this->addSql('DROP TABLE attribute_value');
        $this->addSql('CREATE TABLE attribute_value (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, card_id INTEGER NOT NULL, attribute_id INTEGER NOT NULL, value VARCHAR(255) NOT NULL COLLATE BINARY, value_formatted VARCHAR(255) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_FE4FBB824ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_FE4FBB82B6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO attribute_value (id, card_id, attribute_id, value, value_formatted) SELECT id, card_id, attribute_id, value, value_formatted FROM __temp__attribute_value');
        $this->addSql('DROP TABLE __temp__attribute_value');
        $this->addSql('CREATE INDEX IDX_FE4FBB824ACC9A20 ON attribute_value (card_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82B6E62EFA ON attribute_value (attribute_id)');
        $this->addSql('DROP INDEX IDX_58A92E65F675F31B');
        $this->addSql('CREATE TEMPORARY TABLE __temp__symfony_demo_post AS SELECT id, author_id, title, slug, summary, content, published_at FROM symfony_demo_post');
        $this->addSql('DROP TABLE symfony_demo_post');
        $this->addSql('CREATE TABLE symfony_demo_post (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, author_id INTEGER NOT NULL, title VARCHAR(255) NOT NULL COLLATE BINARY, slug VARCHAR(255) NOT NULL COLLATE BINARY, summary VARCHAR(255) NOT NULL COLLATE BINARY, content CLOB NOT NULL COLLATE BINARY, published_at DATETIME NOT NULL, CONSTRAINT FK_58A92E65F675F31B FOREIGN KEY (author_id) REFERENCES symfony_demo_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO symfony_demo_post (id, author_id, title, slug, summary, content, published_at) SELECT id, author_id, title, slug, summary, content, published_at FROM __temp__symfony_demo_post');
        $this->addSql('DROP TABLE __temp__symfony_demo_post');
        $this->addSql('CREATE INDEX IDX_58A92E65F675F31B ON symfony_demo_post (author_id)');
        $this->addSql('DROP INDEX IDX_6ABC1CC44B89032C');
        $this->addSql('DROP INDEX IDX_6ABC1CC4BAD26311');
        $this->addSql('CREATE TEMPORARY TABLE __temp__symfony_demo_post_tag AS SELECT post_id, tag_id FROM symfony_demo_post_tag');
        $this->addSql('DROP TABLE symfony_demo_post_tag');
        $this->addSql('CREATE TABLE symfony_demo_post_tag (post_id INTEGER NOT NULL, tag_id INTEGER NOT NULL, PRIMARY KEY(post_id, tag_id), CONSTRAINT FK_6ABC1CC44B89032C FOREIGN KEY (post_id) REFERENCES symfony_demo_post (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_6ABC1CC4BAD26311 FOREIGN KEY (tag_id) REFERENCES symfony_demo_tag (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO symfony_demo_post_tag (post_id, tag_id) SELECT post_id, tag_id FROM __temp__symfony_demo_post_tag');
        $this->addSql('DROP TABLE __temp__symfony_demo_post_tag');
        $this->addSql('CREATE INDEX IDX_6ABC1CC44B89032C ON symfony_demo_post_tag (post_id)');
        $this->addSql('CREATE INDEX IDX_6ABC1CC4BAD26311 ON symfony_demo_post_tag (tag_id)');
        $this->addSql('DROP INDEX IDX_DDCC250DB6E62EFA');
        $this->addSql('DROP INDEX IDX_DDCC250D925606E5');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_card_type AS SELECT attribute_id, card_type_id FROM attribute_card_type');
        $this->addSql('DROP TABLE attribute_card_type');
        $this->addSql('CREATE TABLE attribute_card_type (attribute_id INTEGER NOT NULL, card_type_id INTEGER NOT NULL, PRIMARY KEY(attribute_id, card_type_id), CONSTRAINT FK_DDCC250DB6E62EFA FOREIGN KEY (attribute_id) REFERENCES attribute (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_DDCC250D925606E5 FOREIGN KEY (card_type_id) REFERENCES card_type (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO attribute_card_type (attribute_id, card_type_id) SELECT attribute_id, card_type_id FROM __temp__attribute_card_type');
        $this->addSql('DROP TABLE __temp__attribute_card_type');
        $this->addSql('CREATE INDEX IDX_DDCC250DB6E62EFA ON attribute_card_type (attribute_id)');
        $this->addSql('CREATE INDEX IDX_DDCC250D925606E5 ON attribute_card_type (card_type_id)');
        $this->addSql('DROP INDEX IDX_53AD8F834B89032C');
        $this->addSql('DROP INDEX IDX_53AD8F83F675F31B');
        $this->addSql('CREATE TEMPORARY TABLE __temp__symfony_demo_comment AS SELECT id, post_id, author_id, content, published_at FROM symfony_demo_comment');
        $this->addSql('DROP TABLE symfony_demo_comment');
        $this->addSql('CREATE TABLE symfony_demo_comment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, post_id INTEGER NOT NULL, author_id INTEGER NOT NULL, content CLOB NOT NULL COLLATE BINARY, published_at DATETIME NOT NULL, CONSTRAINT FK_53AD8F834B89032C FOREIGN KEY (post_id) REFERENCES symfony_demo_post (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_53AD8F83F675F31B FOREIGN KEY (author_id) REFERENCES symfony_demo_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO symfony_demo_comment (id, post_id, author_id, content, published_at) SELECT id, post_id, author_id, content, published_at FROM __temp__symfony_demo_comment');
        $this->addSql('DROP TABLE __temp__symfony_demo_comment');
        $this->addSql('CREATE INDEX IDX_53AD8F834B89032C ON symfony_demo_comment (post_id)');
        $this->addSql('CREATE INDEX IDX_53AD8F83F675F31B ON symfony_demo_comment (author_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_DDCC250DB6E62EFA');
        $this->addSql('DROP INDEX IDX_DDCC250D925606E5');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_card_type AS SELECT attribute_id, card_type_id FROM attribute_card_type');
        $this->addSql('DROP TABLE attribute_card_type');
        $this->addSql('CREATE TABLE attribute_card_type (attribute_id INTEGER NOT NULL, card_type_id INTEGER NOT NULL, PRIMARY KEY(attribute_id, card_type_id))');
        $this->addSql('INSERT INTO attribute_card_type (attribute_id, card_type_id) SELECT attribute_id, card_type_id FROM __temp__attribute_card_type');
        $this->addSql('DROP TABLE __temp__attribute_card_type');
        $this->addSql('CREATE INDEX IDX_DDCC250DB6E62EFA ON attribute_card_type (attribute_id)');
        $this->addSql('CREATE INDEX IDX_DDCC250D925606E5 ON attribute_card_type (card_type_id)');
        $this->addSql('DROP INDEX IDX_FE4FBB824ACC9A20');
        $this->addSql('DROP INDEX IDX_FE4FBB82B6E62EFA');
        $this->addSql('CREATE TEMPORARY TABLE __temp__attribute_value AS SELECT id, card_id, attribute_id, value, value_formatted FROM attribute_value');
        $this->addSql('DROP TABLE attribute_value');
        $this->addSql('CREATE TABLE attribute_value (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, card_id INTEGER NOT NULL, attribute_id INTEGER NOT NULL, value VARCHAR(255) NOT NULL, value_formatted VARCHAR(255) DEFAULT NULL)');
        $this->addSql('INSERT INTO attribute_value (id, card_id, attribute_id, value, value_formatted) SELECT id, card_id, attribute_id, value, value_formatted FROM __temp__attribute_value');
        $this->addSql('DROP TABLE __temp__attribute_value');
        $this->addSql('CREATE INDEX IDX_FE4FBB824ACC9A20 ON attribute_value (card_id)');
        $this->addSql('CREATE INDEX IDX_FE4FBB82B6E62EFA ON attribute_value (attribute_id)');
        $this->addSql('DROP INDEX UNIQ_161498D3989D9B62');
        $this->addSql('DROP INDEX IDX_161498D3C54C8C93');
        $this->addSql('CREATE TEMPORARY TABLE __temp__card AS SELECT id, type_id, title, image, description, slug, content, short_description FROM card');
        $this->addSql('DROP TABLE card');
        $this->addSql('CREATE TABLE card (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type_id INTEGER NOT NULL, title VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, content CLOB DEFAULT NULL, short_description VARCHAR(255) DEFAULT NULL)');
        $this->addSql('INSERT INTO card (id, type_id, title, image, description, slug, content, short_description) SELECT id, type_id, title, image, description, slug, content, short_description FROM __temp__card');
        $this->addSql('DROP TABLE __temp__card');
        $this->addSql('CREATE INDEX IDX_161498D3C54C8C93 ON card (type_id)');
        $this->addSql('DROP INDEX IDX_53AD8F834B89032C');
        $this->addSql('DROP INDEX IDX_53AD8F83F675F31B');
        $this->addSql('CREATE TEMPORARY TABLE __temp__symfony_demo_comment AS SELECT id, post_id, author_id, content, published_at FROM symfony_demo_comment');
        $this->addSql('DROP TABLE symfony_demo_comment');
        $this->addSql('CREATE TABLE symfony_demo_comment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, post_id INTEGER NOT NULL, author_id INTEGER NOT NULL, content CLOB NOT NULL, published_at DATETIME NOT NULL)');
        $this->addSql('INSERT INTO symfony_demo_comment (id, post_id, author_id, content, published_at) SELECT id, post_id, author_id, content, published_at FROM __temp__symfony_demo_comment');
        $this->addSql('DROP TABLE __temp__symfony_demo_comment');
        $this->addSql('CREATE INDEX IDX_53AD8F834B89032C ON symfony_demo_comment (post_id)');
        $this->addSql('CREATE INDEX IDX_53AD8F83F675F31B ON symfony_demo_comment (author_id)');
        $this->addSql('DROP INDEX IDX_58A92E65F675F31B');
        $this->addSql('CREATE TEMPORARY TABLE __temp__symfony_demo_post AS SELECT id, author_id, title, slug, summary, content, published_at FROM symfony_demo_post');
        $this->addSql('DROP TABLE symfony_demo_post');
        $this->addSql('CREATE TABLE symfony_demo_post (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, author_id INTEGER NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, summary VARCHAR(255) NOT NULL, content CLOB NOT NULL, published_at DATETIME NOT NULL)');
        $this->addSql('INSERT INTO symfony_demo_post (id, author_id, title, slug, summary, content, published_at) SELECT id, author_id, title, slug, summary, content, published_at FROM __temp__symfony_demo_post');
        $this->addSql('DROP TABLE __temp__symfony_demo_post');
        $this->addSql('CREATE INDEX IDX_58A92E65F675F31B ON symfony_demo_post (author_id)');
        $this->addSql('DROP INDEX IDX_6ABC1CC44B89032C');
        $this->addSql('DROP INDEX IDX_6ABC1CC4BAD26311');
        $this->addSql('CREATE TEMPORARY TABLE __temp__symfony_demo_post_tag AS SELECT post_id, tag_id FROM symfony_demo_post_tag');
        $this->addSql('DROP TABLE symfony_demo_post_tag');
        $this->addSql('CREATE TABLE symfony_demo_post_tag (post_id INTEGER NOT NULL, tag_id INTEGER NOT NULL, PRIMARY KEY(post_id, tag_id))');
        $this->addSql('INSERT INTO symfony_demo_post_tag (post_id, tag_id) SELECT post_id, tag_id FROM __temp__symfony_demo_post_tag');
        $this->addSql('DROP TABLE __temp__symfony_demo_post_tag');
        $this->addSql('CREATE INDEX IDX_6ABC1CC44B89032C ON symfony_demo_post_tag (post_id)');
        $this->addSql('CREATE INDEX IDX_6ABC1CC4BAD26311 ON symfony_demo_post_tag (tag_id)');
    }
}
