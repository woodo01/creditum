<?php

namespace App\Utils;

class Calculator
{
    public function calc($creditSum, $time, $rate)
    {
        $months = [];

        $currentMonth = time();
        $rest = $creditSum;
        $total = (1 + $rate * $time / 12 / 100) * $creditSum;
        $monthSum = $total / $time;

        $table = [
            date('d.m.Y', $currentMonth) => [
                'rest' => $rest,
                'procSum' => 0,
                'creditSum' => 0,
                'monthSum' => 0,
            ]
        ];

        $totalProcSum = $total - $creditSum;
        $procSum = $totalProcSum / $time;
        $creditSum = $creditSum / $time;

        if ($time == 1) {
            $koef = 0;
            $step = 0;
        } else {
            $koef = .1;
            $step = 2 * $koef / ($time - 1);
        }

        for ($i = 0; $i < $time; $i++) {
            $currentMonth = strtotime("+1 month", $currentMonth);
            $months[] =  date('d.m.Y', $currentMonth);
            $table[date('d.m.Y', $currentMonth)] = [
                'rest' => $rest,
                'procSum' => $procSum * (1 + $koef),
                'creditSum' => $creditSum * ( 1 - $koef),
                'monthSum' => $monthSum,
            ];
            $procSums[] = $procSum * (1 + $koef);
            $creditSums[] = $creditSum * ( 1 - $koef);
            $rest -= $creditSum;
            $restSums[] = $rest;
            $koef -= $step;
        }

        return [
            'cards' => [],
            'table' => $table,
            'chart' => [
                'months' => $months,
                'dataSets' => [
                    'procSums' => $procSums,
                    'creditSums' => $creditSums,
                    'restSums' => $restSums
                ]
            ]
        ];
    }
}
