<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilterRepository")
 */
class Filter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardType", inversedBy="filters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cardType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FilterValue", mappedBy="filter", cascade={"persist"}, orphanRemoval=true)
     */
    private $filterValues;

    public function __construct()
    {
        $this->filterValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCardType(): ?CardType
    {
        return $this->cardType;
    }

    public function setCardType(?CardType $cardType): self
    {
        $this->cardType = $cardType;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @return Collection|FilterValue[]
     */
    public function getFilterValues(): Collection
    {
        return $this->filterValues;
    }

    public function addFilterValue(FilterValue $filterValue): self
    {
        if (!$this->filterValues->contains($filterValue)) {
            $this->filterValues[] = $filterValue;
            $filterValue->setFilter($this);
        }

        return $this;
    }

    public function removeFilterValue(FilterValue $filterValue): self
    {
        if ($this->filterValues->contains($filterValue)) {
            $this->filterValues->removeElement($filterValue);
            // set the owning side to null (unless already changed)
            if ($filterValue->getFilter() === $this) {
                $filterValue->setFilter(null);
            }
        }

        return $this;
    }

    public function setFilterValues(Collection $collection): self
    {
        $this->filterValues = $collection;

        return $this;
    }
}
