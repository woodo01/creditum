<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @UniqueEntity(fields={"slug"}, message="This slug is already exists!")
 * @ORM\Entity(repositoryClass="App\Repository\CardRepository")
 * @Vich\Uploadable
 */
class Card
{
    public const DEBIT_CARD = 1;
    public const CREDIT_CARD = 2;
    public const INSTALLMENT_CARD = 3;
    public const VIRTUAL_CARD = 4;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CardType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="card_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AttributeValue", mappedBy="card", orphanRemoval=true)
     */
    private $attributeValues;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FilterValue", inversedBy="cards")
     */
    private $filterValues;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $creditLimitFrom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $creditLimitTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $creditLimitFormatted;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $issuePeriodFrom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $issuePeriodTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $issuePeriodFormatted;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cashbackFrom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cashbackTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cashbackFormatted;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $gracePeriodFrom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $gracePeriodTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gracePeriodFormatted;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $annualRateFrom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $annualRateTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $annualRateFormatted;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $serviceCostFrom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $serviceCostTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $serviceCostFormatted;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $interestOnBalanceFrom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $interestOnBalanceTo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $interestOnBalanceFormatted;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->attributeValues = new ArrayCollection();
        $this->filterValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?CardType
    {
        return $this->type;
    }

    public function setType(CardType $type)
    {
        $this->type = $type;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Card
     */
    public function setSlug(string $slug): Card
    {
        $this->slug = $slug;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|AttributeValue[]
     */
    public function getAttributeValues(): Collection
    {
        return $this->attributeValues;
    }

    public function addAttributeValue(AttributeValue $attributeValue): self
    {
        if (!$this->attributeValues->contains($attributeValue)) {
            $this->attributeValues[] = $attributeValue;
            $attributeValue->setCard($this);
        }

        return $this;
    }

    public function removeAttributeValue(AttributeValue $attributeValue): self
    {
        if ($this->attributeValues->contains($attributeValue)) {
            $this->attributeValues->removeElement($attributeValue);
            // set the owning side to null (unless already changed)
            if ($attributeValue->getCard() === $this) {
                $attributeValue->setCard(null);
            }
        }

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * @return Collection|FilterValue[]
     */
    public function getFilterValues(): Collection
    {
        return $this->filterValues;
    }

    public function addFilterValue(FilterValue $filterValue): self
    {
        if (!$this->filterValues->contains($filterValue)) {
            $this->filterValues[] = $filterValue;
        }

        return $this;
    }

    public function removeFilterValue(FilterValue $filterValue): self
    {
        if ($this->filterValues->contains($filterValue)) {
            $this->filterValues->removeElement($filterValue);
        }

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getName(): string
    {
        return '';
    }

    public function getCreditLimitFrom(): ?float
    {
        return $this->creditLimitFrom;
    }

    public function setCreditLimitFrom(?float $creditLimitFrom): self
    {
        $this->creditLimitFrom = $creditLimitFrom;

        return $this;
    }

    public function getCreditLimitTo(): ?float
    {
        return $this->creditLimitTo;
    }

    public function setCreditLimitTo(?float $creditLimitTo): self
    {
        $this->creditLimitTo = $creditLimitTo;

        return $this;
    }

    public function getCreditLimitFormatted(): ?string
    {
        return $this->creditLimitFormatted;
    }

    public function setCreditLimitFormatted(?string $creditLimitFormatted): self
    {
        $this->creditLimitFormatted = $creditLimitFormatted;

        return $this;
    }

    public function getIssuePeriodFrom(): ?float
    {
        return $this->issuePeriodFrom;
    }

    public function setIssuePeriodFrom(?float $issuePeriodFrom): self
    {
        $this->issuePeriodFrom = $issuePeriodFrom;

        return $this;
    }

    public function getIssuePeriodTo(): ?float
    {
        return $this->issuePeriodTo;
    }

    public function setIssuePeriodTo(?float $issuePeriodTo): self
    {
        $this->issuePeriodTo = $issuePeriodTo;

        return $this;
    }

    public function getIssuePeriodFormatted(): ?string
    {
        return $this->issuePeriodFormatted;
    }

    public function setIssuePeriodFormatted(?string $issuePeriodFormatted): self
    {
        $this->issuePeriodFormatted = $issuePeriodFormatted;

        return $this;
    }

    public function getCashbackFrom(): ?float
    {
        return $this->cashbackFrom;
    }

    public function setCashbackFrom(?float $cashbackFrom): self
    {
        $this->cashbackFrom = $cashbackFrom;

        return $this;
    }

    public function getCashbackTo(): ?float
    {
        return $this->cashbackTo;
    }

    public function setCashbackTo(?float $cashbackTo): self
    {
        $this->cashbackTo = $cashbackTo;

        return $this;
    }

    public function getCashbackFormatted(): ?string
    {
        return $this->cashbackFormatted;
    }

    public function setCashbackFormatted(?string $cashbackFormatted): self
    {
        $this->cashbackFormatted = $cashbackFormatted;

        return $this;
    }

    public function getGracePeriodFrom(): ?float
    {
        return $this->gracePeriodFrom;
    }

    public function setGracePeriodFrom(?float $gracePeriodFrom): self
    {
        $this->gracePeriodFrom = $gracePeriodFrom;

        return $this;
    }

    public function getGracePeriodTo(): ?float
    {
        return $this->gracePeriodTo;
    }

    public function setGracePeriodTo(?float $gracePeriodTo): self
    {
        $this->gracePeriodTo = $gracePeriodTo;

        return $this;
    }

    public function getGracePeriodFormatted(): ?string
    {
        return $this->gracePeriodFormatted;
    }

    public function setGracePeriodFormatted(?string $gracePeriodFormatted): self
    {
        $this->gracePeriodFormatted = $gracePeriodFormatted;

        return $this;
    }

    public function getAnnualRateFrom(): ?float
    {
        return $this->annualRateFrom;
    }

    public function setAnnualRateFrom(?float $annualRateFrom): self
    {
        $this->annualRateFrom = $annualRateFrom;

        return $this;
    }

    public function getAnnualRateTo(): ?float
    {
        return $this->annualRateTo;
    }

    public function setAnnualRateTo(?float $annualRateTo): self
    {
        $this->annualRateTo = $annualRateTo;

        return $this;
    }

    public function getAnnualRateFormatted(): ?string
    {
        return $this->annualRateFormatted;
    }

    public function setAnnualRateFormatted(?string $annualRateFormatted): self
    {
        $this->annualRateFormatted = $annualRateFormatted;

        return $this;
    }

    public function getServiceCostFrom(): ?float
    {
        return $this->serviceCostFrom;
    }

    public function setServiceCostFrom(?float $serviceCostFrom): self
    {
        $this->serviceCostFrom = $serviceCostFrom;

        return $this;
    }

    public function getServiceCostTo(): ?float
    {
        return $this->serviceCostTo;
    }

    public function setServiceCostTo(?float $serviceCostTo): self
    {
        $this->serviceCostTo = $serviceCostTo;

        return $this;
    }

    public function getServiceCostFormatted(): ?string
    {
        return $this->serviceCostFormatted;
    }

    public function setServiceCostFormatted(string $serviceCostFormatted): self
    {
        $this->serviceCostFormatted = $serviceCostFormatted;

        return $this;
    }

    public function getInterestOnBalanceFrom(): ?float
    {
        return $this->interestOnBalanceFrom;
    }

    public function setInterestOnBalanceFrom(?float $interestOnBalanceFrom): self
    {
        $this->interestOnBalanceFrom = $interestOnBalanceFrom;

        return $this;
    }

    public function getInterestOnBalanceTo(): ?float
    {
        return $this->interestOnBalanceTo;
    }

    public function setInterestOnBalanceTo(?float $interestOnBalanceTo): self
    {
        $this->interestOnBalanceTo = $interestOnBalanceTo;

        return $this;
    }

    public function getInterestOnBalanceFormatted(): ?string
    {
        return $this->interestOnBalanceFormatted;
    }

    public function setInterestOnBalanceFormatted(?string $interestOnBalanceFormatted): self
    {
        $this->interestOnBalanceFormatted = $interestOnBalanceFormatted;

        return $this;
    }
}
