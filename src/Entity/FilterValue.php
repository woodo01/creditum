<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilterValueRepository")
 */
class FilterValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Filter", inversedBy="filterValues", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $filter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Card", mappedBy="filterValues")
     */
    private $cards;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PromoText", mappedBy="filterValues")
     */
    private $promoTexts;

    /**
     * FilterValue constructor.
     * @param $filter
     * @param $value
     */
    public function __construct($filter, $value)
    {
        $this->filter = $filter;
        $this->setValue($value);
        $this->cards = new ArrayCollection();
        $this->promoTexts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilter(): ?Filter
    {
        return $this->filter;
    }

    public function setFilter(?Filter $filter): self
    {
        $this->filter = $filter;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = str_replace($this->getPrefix(), '', $value);

        return $this;
    }

    private function getPrefix(): string
    {
        return $this->getFilter()->getCardType()->getName() . ': ' . $this->getFilter()->getName() . ' - ';
    }

    public function __toString(): string
    {
        return $this->getPrefix() . $this->getValue();
    }

    /**
     * @return Collection|Card[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(Card $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->addFilterValue($this);
        }

        return $this;
    }

    public function removeCard(Card $card): self
    {
        if ($this->cards->contains($card)) {
            $this->cards->removeElement($card);
            $card->removeFilterValue($this);
        }

        return $this;
    }

    /**
     * @return Collection|PromoText[]
     */
    public function getPromoTexts(): Collection
    {
        return $this->promoTexts;
    }

    public function addPromoText(PromoText $promoText): self
    {
        if (!$this->promoTexts->contains($promoText)) {
            $this->promoTexts[] = $promoText;
            $promoText->addFilterValue($this);
        }

        return $this;
    }

    public function removePromoText(PromoText $promoText): self
    {
        if ($this->promoTexts->contains($promoText)) {
            $this->promoTexts->removeElement($promoText);
            $promoText->removeFilterValue($this);
        }

        return $this;
    }
}
