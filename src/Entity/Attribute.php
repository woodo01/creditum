<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AttributeRepository")
 */
class Attribute
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CardType", inversedBy="attributes")
     */
    private $cardTypes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $priority;

    /**
     * @ORM\Column(type="boolean")
     */
    private $inFilter;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AttributeValue", mappedBy="attribute", orphanRemoval=true)
     */
    private $attributeValues;

    public function __construct()
    {
        $this->cardTypes = new ArrayCollection();
        $this->attributeValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|CardType[]
     */
    public function getCardTypes(): Collection
    {
        return $this->cardTypes;
    }

    public function addCardType(CardType $cardType): self
    {
        if (!$this->cardTypes->contains($cardType)) {
            $this->cardTypes[] = $cardType;
        }

        return $this;
    }

    public function removeCardType(CardType $cardType): self
    {
        if ($this->cardTypes->contains($cardType)) {
            $this->cardTypes->removeElement($cardType);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getInFilter(): ?bool
    {
        return $this->inFilter;
    }

    public function setInFilter(bool $inFilter): self
    {
        $this->inFilter = $inFilter;

        return $this;
    }

    /**
     * @return Collection|AttributeValue[]
     */
    public function getAttributeValues(): Collection
    {
        return $this->attributeValues;
    }

    public function addAttributeValue(AttributeValue $attributeValue): self
    {
        if (!$this->attributeValues->contains($attributeValue)) {
            $this->attributeValues[] = $attributeValue;
            $attributeValue->setAttribute($this);
        }

        return $this;
    }

    public function removeAttributeValue(AttributeValue $attributeValue): self
    {
        if ($this->attributeValues->contains($attributeValue)) {
            $this->attributeValues->removeElement($attributeValue);
            // set the owning side to null (unless already changed)
            if ($attributeValue->getAttribute() === $this) {
                $attributeValue->setAttribute(null);
            }
        }

        return $this;
    }

    public function getFormattedCardTypes(): string
    {
        $arr = array_map(function(CardType $item) { return $item->getName();}, $this->cardTypes->toArray());
        return implode(', ', $arr);
    }
}
