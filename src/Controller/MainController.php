<?php


namespace App\Controller;


use App\Entity\Card;
use App\Repository\CardRepository;
use App\Repository\MainContentRepository;
use App\Repository\MainSliderRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="main_index")
     *
     * @param MainSliderRepository $sliderRepository
     * @param CardRepository $cardRepository
     * @param MainContentRepository $mainContentRepository
     * @param NewsRepository $newsRepository
     * @return Response
     */
    public function index(MainSliderRepository $sliderRepository, CardRepository $cardRepository,
                          MainContentRepository $mainContentRepository, NewsRepository $newsRepository): Response
    {
        return $this->render('main/index.html.twig', [
            'slides' => $sliderRepository->findBy([], ['sort' => 'asc']),
            'credit_cards' => $cardRepository->findBy(['type' => Card::CREDIT_CARD], [], 4),
            'debit_cards' => $cardRepository->findBy(['type' => Card::DEBIT_CARD], [], 4),
            'installment_cards' => $cardRepository->findBy(['type' => Card::INSTALLMENT_CARD], [], 4),
            'virtual_cards' => $cardRepository->findBy(['type' => Card::VIRTUAL_CARD], [], 4),
            'news' => $newsRepository->findBy([], [], 4),
            'content' => $mainContentRepository->find(1)->getContent()
        ]);
    }
}