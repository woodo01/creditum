<?php

namespace App\Controller;

use App\Entity\FilterValue;
use App\Entity\News;
use App\Entity\PromoText;
use App\Form\News1Type;
use App\Repository\CardRepository;
use App\Repository\FilterRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/promotext")
 */
class PromotextController extends AbstractController
{
    /**
     * @Route("/{slug}", name="promotext_index", methods={"GET"})
     * @param PromoText $promoText
     * @param Request $request
     * @param CardRepository $cardRepository
     * @param FilterRepository $filterRepository
     * @return Response
     */
    public function index(PromoText $promoText, Request $request, CardRepository $cardRepository, FilterRepository $filterRepository): Response
    {
        $filters = $promoText->getFilterValues();

        if ($request->query->has('cardUrl')) {
            $params = [];

            if ($request->query->get('filter')) {
                $params = $request->query->get('filter');
            }

            return $this->redirectToRoute($request->query->get('cardUrl'), ['filter' => $params], 301);
        }

        $filterValues = [];

        /** @var FilterValue $filterValue */
        foreach ($filters as $filterValue) {
            $filterValues[] = $filterValue->getId();
            $cardType = $filterValue->getFilter()->getCardType();
        }

        $cardTypeId = isset($cardType) ? $cardType->getId() : 0;
        switch ($cardTypeId) {
            case 1:
                $cardTypeUrl = 'card_debit';
                break;
            case 2:
                $cardTypeUrl = 'card_credit';
                break;
            case 3:
                $cardTypeUrl = 'card_installment';
                break;
            case 4:
                $cardTypeUrl = 'card_virtual';
                break;
            default:
                $cardTypeUrl = 'card_index';

        }

        $cards = $cardRepository->getCards($cardTypeId, $filterValues);
        $filter = $filterRepository->getFilter($cardTypeId);
        return $this->render('card/index.html.twig', [
            'cards' => $cards,
            'cardType' => $cardType ?? null,
            'filter' => $filter,
            'filterChecked' => $filterValues,
            'promoText' => null,
            'h1' => $promoText->getH1(),
            'promo_content' => $promoText->getContent(),
            'title' => $promoText->getTitle(),
            'cardUrl' => $cardTypeUrl,
            'children' => $promoText->getChildren()
        ]);
    }
}
