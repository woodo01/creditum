<?php

namespace App\Controller\Admin;

use App\Entity\CardType;
use App\Form\CardtypeType;
use App\Repository\CardTypeRepository;
use App\Security\CardTypeVoter;
use App\Utils\Slugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * See http://knpbundles.com/keyword/admin
 *
 * @Route("/admin/cardtype")
 * @IsGranted("ROLE_ADMIN")
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class CardTypeController extends AbstractController
{
    /**
     * Lists all Post entities.
     *
     * This controller responds to two different routes with the same URL:
     *   * 'admin_post_index' is the route with a name that follows the same
     *     structure as the rest of the controllers of this class.
     *   * 'admin_index' is a nice shortcut to the backend homepage. This allows
     *     to create simpler links in the templates. Moreover, in the future we
     *     could move this annotation to any other controller while maintaining
     *     the route name and therefore, without breaking any existing link.
     *
     * @Route("/", methods={"GET"}, name="admin_card_type_index")
     * @param CardTypeRepository $cardTypes
     * @return Response
     */
    public function index(CardTypeRepository $cardTypes): Response
    {
        return $this->render('admin/card_type/index.html.twig', ['card_types' => $cardTypes->findBy([], ['name' => 'asc'])]);
    }

    /**
     * Creates a new Post entity.
     *
     * @Route("/new", methods={"GET", "POST"}, name="admin_card_type_new")
     *
     * NOTE: the Method annotation is optional, but it's a recommended practice
     * to constraint the HTTP methods each controller responds to (by default
     * it responds to all methods).
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $cardType = new CardType();

        // See https://symfony.com/doc/current/book/forms.html#submitting-forms-with-multiple-buttons
        $form = $this->createForm(CardtypeType::class, $cardType)
            ->add('saveAndCreateNew', SubmitType::class);

        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/best_practices/forms.html#handling-form-submits
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($cardType);
            $em->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', 'card_type.created_successfully');

            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('admin_card_type_new');
            }

            return $this->redirectToRoute('admin_card_type_index');
        }

        return $this->render('admin/card_type/new.html.twig', [
            'cardType' => $cardType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id<\d+>}/edit",methods={"GET", "POST"}, name="admin_card_type_edit")
     * @IsGranted("edit", subject="cardType", message="Posts can only be edited by their authors.")
     * @param Request $request
     * @param CardType $cardType
     * @return Response
     */
    public function edit(Request $request, CardType $cardType): Response
    {
        $form = $this->createForm(CardtypeType::class, $cardType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'card_type.updated_successfully');

            return $this->redirectToRoute('admin_card_type_edit', ['id' => $cardType->getId()]);
        }

        return $this->render('admin/card_type/edit.html.twig', [
            'cardType' => $cardType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/{id}/delete", methods={"POST"}, name="admin_card_type_delete")
     * @IsGranted("delete", subject="cardType")
     * @param Request $request
     * @param CardType $cardType
     * @return Response
     */
    public function delete(Request $request, CardType $cardType): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin_card_type_index');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($cardType);
        $em->flush();

        $this->addFlash('success', 'card_type.deleted_successfully');

        return $this->redirectToRoute('admin_card_type_index');
    }
}
