<?php
namespace App\Controller\Admin;


use App\Entity\Card;
use App\Entity\Filter;
use App\Entity\FilterValue;
use App\Repository\CardRepository;
use App\Utils\Slugger;
use Doctrine\Common\Collections\ArrayCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/easyadmin")
 *
 */
class AdminController extends EasyAdminController
{
    private $cardRepository;

    /**
     * AdminController constructor.
     * @param CardRepository $cardRepository
     */
    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    public function persistEntity($entity)
    {
        if (method_exists($entity, 'setCreated')) {
            $entity->setCreated(new \DateTime('now'));
        }
        $this->updateSlug($entity);
        $this->fillFilter($entity);
        parent::persistEntity($entity);
    }

    public function updateEntity($entity)
    {
        $this->updateSlug($entity, 1);
        $this->fillFilter($entity);
        parent::updateEntity($entity);
    }

    private function updateSlug($entity, $threshold = 0)
    {
        if (method_exists($entity, 'setSlug')) {
            $slug = $entity->getSlug();

            if (!$slug) {
                if (method_exists($entity, 'getTitle')) {
                    $slug = Slugger::slugify($entity->getTitle());
                } else if (method_exists($entity, 'getName')) {
                    $slug = Slugger::slugify($entity->getName());
                }
            }

            $countSlug = $this->countSlug($entity, $slug);
            $counter = $countSlug - 1;

            while ($countSlug > $threshold) {
                $counter++;
                $countSlug = $this->countSlug($entity, $slug . $counter);
            }

            $entity->setSlug($slug . ($counter > $threshold ?: ''));
        }
    }

    private function countSlug($entity, string $slug): int
    {
        if ($entity instanceof Card) {
            return $this->cardRepository->countSlug($slug);
        }

        return 0;
    }

    /**
     * @param $entity
     */
    protected function fillFilter($entity): void
    {
        /** @var Filter $entity */
        if ($entity instanceof Filter) {
            $newCollection = new ArrayCollection();

            foreach ($entity->getFilterValues() as $item) {
                if (is_string($item)) {
                    $newCollection->add(new FilterValue($entity, $item));
                } else {
                    $newCollection->add($item);
                }
            }

            $entity->setFilterValues($newCollection);
        }
    }

}