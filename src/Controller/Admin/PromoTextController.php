<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\PromoText;
use App\Form\PromoTextType;
use App\Repository\AttributeValueRepository;
use App\Repository\PromoTextRepository;
use App\Security\PostVoter;
use App\Utils\Slugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * See http://knpbundles.com/keyword/admin
 *
 * @Route("/admin/promo_text")
 * @IsGranted("ROLE_ADMIN")
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class PromoTextController extends AbstractController
{
    /**
     * Lists all Post entities.
     *
     * This controller responds to two different routes with the same URL:
     *   * 'admin_post_index' is the route with a name that follows the same
     *     structure as the rest of the controllers of this class.
     *   * 'admin_index' is a nice shortcut to the backend homepage. This allows
     *     to create simpler links in the templates. Moreover, in the future we
     *     could move this annotation to any other controller while maintaining
     *     the route name and therefore, without breaking any existing link.
     *
     * @Route("/", methods={"GET"}, name="admin_promo_text_index")
     * @param PromoTextRepository $promoText
     * @return Response
     */
    public function index(PromoTextRepository $promoText): Response
    {
        $selectedPromoText = $promoText->findBy([], ['title' => 'ASC']);

        return $this->render('admin/promo_text/index.html.twig', ['promo_texts' => $selectedPromoText]);
    }

    /**
     * Creates a new Post entity.
     *
     * @Route("/new", methods={"GET", "POST"}, name="admin_promo_text_new")
     *
     * NOTE: the Method annotation is optional, but it's a recommended practice
     * to constraint the HTTP methods each controller responds to (by default
     * it responds to all methods).
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $promoText = new PromoText();

        // See https://symfony.com/doc/current/book/forms.html#submitting-forms-with-multiple-buttons
        $form = $this->createForm(PromoTextType::class, $promoText)
            ->add('saveAndCreateNew', SubmitType::class);

        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/best_practices/forms.html#handling-form-submits
        if ($form->isSubmitted() && $form->isValid()) {
            $promoText->setSlug(Slugger::slugify($promoText->getTitle()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($promoText);
            $em->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', 'promo_text_.created_successfully');

            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('admin_promo_text_new');
            }

            return $this->redirectToRoute('admin_promo_text_index');
        }

        return $this->render('admin/promo_text/new.html.twig', [
            'promo_text_' => $promoText,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id<\d+>}/edit",methods={"GET", "POST"}, name="admin_promo_text_edit")
     * @param Request $request
     * @param PromoText $promoText
     * @return Response
     */
    public function edit(Request $request, PromoText $promoText): Response
    {
        $form = $this->createForm(PromoTextType::class, $promoText);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $promoText->setSlug(Slugger::slugify($promoText->getTitle()));

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'promo_text.updated_successfully');
            return $this->redirectToRoute('admin_promo_text_edit', ['id' => $promoText->getId()]);
        }

        return $this->render('admin/promo_text/edit.html.twig', [
            'promo_text' => $promoText,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/{id}/delete", methods={"POST"}, name="admin_promo_text_delete")
     * @param Request $request
     * @param PromoText $promoText
     * @return Response
     */
    public function delete(Request $request, PromoText $promoText): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin_promo_text_index');
        }

        // Delete the tags associated with this blog post. This is done automatically
        // by Doctrine, except for SQLite (the database used in this application)
        // because foreign key support is not enabled by default in SQLite
//        $promoText->getTags()->clear();

        $em = $this->getDoctrine()->getManager();
        $em->remove($promoText);
        $em->flush();

        $this->addFlash('success', 'promo_text.deleted_successfully');

        return $this->redirectToRoute('admin_promo_text_index');
    }
}
