<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Attribute;
use App\Entity\AttributeValue;
use App\Entity\Card;
use App\Form\AttributeType;
use App\Form\AttributeValueType;
use App\Repository\AttributeRepository;
use App\Repository\AttributeValueRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * See http://knpbundles.com/keyword/admin
 *
 * @Route("/admin/attributevalue")
 * @IsGranted("ROLE_ADMIN")
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class AttributeValueController extends AbstractController
{
    /**
     * Lists all Post entities.
     *
     * This controller responds to two different routes with the same URL:
     *   * 'admin_post_index' is the route with a name that follows the same
     *     structure as the rest of the controllers of this class.
     *   * 'admin_index' is a nice shortcut to the backend homepage. This allows
     *     to create simpler links in the templates. Moreover, in the future we
     *     could move this annotation to any other controller while maintaining
     *     the route name and therefore, without breaking any existing link.
     *
     * @Route("/{cardId<\d+>}/{attributeId<\d+>}/", methods={"GET"}, name="admin_attributevalue_index")
     * @ParamConverter("card", options={"mapping": {"cardId" : "id"}})
     * @ParamConverter("attribute", options={"mapping": {"attributeId"   : "id"}})
     * @param AttributeValueRepository $repository
     * @param Card $card
     * @param Attribute $attribute
     * @return Response
     */
    public function index(AttributeValueRepository $repository, Card $card, Attribute $attribute): Response
    {
        $attributeValues = $repository->findBy(['card' => $card, 'attribute' => $attribute], ['value' => 'ASC']);
        return $this->render('admin/attributevalue/index.html.twig', ['attributeValues' => $attributeValues, 'card' => $card, 'attribute' => $attribute]);
    }

    /**
     * Creates a new Post entity.
     *
     * @Route("/{cardId<\d+>}/{attributeId<\d+>}/new", methods={"GET", "POST"}, name="admin_attributevalue_new")
     * @ParamConverter("card", options={"mapping": {"cardId" : "id"}})
     * @ParamConverter("attribute", options={"mapping": {"attributeId"   : "id"}})
     * NOTE: the Method annotation is optional, but it's a recommended practice
     * to constraint the HTTP methods each controller responds to (by default
     * it responds to all methods).
     * @param Request $request
     * @param Card $card
     * @param Attribute $attributeValue
     * @return Response
     */
    public function new(Request $request, Card $card, Attribute $attribute): Response
    {
        $attributeValue = new AttributeValue();
        $attributeValue->setCard($card);
        $attributeValue->setAttribute($attribute);

        // See https://symfony.com/doc/current/book/forms.html#submitting-forms-with-multiple-buttons
        $form = $this->createForm(AttributeValueType::class, $attributeValue)
            ->add('saveAndCreateNew', SubmitType::class);

        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/best_practices/forms.html#handling-form-submits
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($attributeValue);
            $em->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', 'attribute.created_successfully');

            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('admin_attributevalue_new', ['cardId' => $card->getId(), 'attributeId' => $attribute->getId()]);
            }

            return $this->redirectToRoute('admin_attributevalue_index', ['cardId' => $card->getId(), 'attributeId' => $attribute->getId()]);
        }

        return $this->render('admin/attributevalue/new.html.twig', [
            'attributeValue' => $attributeValue,
            'card' => $card,
            'attribute' => $attribute,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id<\d+>}/edit",methods={"GET", "POST"}, name="admin_attributevalue_edit")
     * @param Request $request
     * @param AttributeValue $attributeValue
     * @return Response
     */
    public function edit(Request $request, AttributeValue $attributeValue): Response
    {
        $form = $this->createForm(AttributeValueType::class, $attributeValue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'attributevalue.updated_successfully');

            return $this->redirectToRoute('admin_attributevalue_edit', ['id' => $attributeValue->getId()]);
        }

        return $this->render('admin/attributevalue/edit.html.twig', [
            'attributeValue' => $attributeValue,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/{id}/delete", methods={"POST"}, name="admin_attributevalue_delete")
     * @param Request $request
     * @param AttributeValue $attributeValue
     * @return Response
     */
    public function delete(Request $request, AttributeValue $attributeValue): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin_attributevalue_index', ['cardId' => $attributeValue->getCard()->getId(), 'attributeId' => $attributeValue->getAttribute()->getId()]);
        }

        // Delete the tags associated with this blog post. This is done automatically
        // by Doctrine, except for SQLite (the database used in this application)
        // because foreign key support is not enabled by default in SQLite
//        $attribute->getTags()->clear();

        $em = $this->getDoctrine()->getManager();
        $em->remove($attributeValue);
        $em->flush();

        $this->addFlash('success', 'attributevalue.deleted_successfully');

        return $this->redirectToRoute('admin_attributevalue_index', ['cardId' => $attributeValue->getCard()->getId(), 'attributeId' => $attributeValue->getAttribute()->getId()]);
    }
}
