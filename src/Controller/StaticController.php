<?php
/**
 * Created by IntelliJ IDEA.
 * User: ivansigaev
 * Date: 2019-06-17
 * Time: 21:09
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StaticController extends AbstractController
{
    /**
     * @Route("/about", methods="GET", name="about")
     * @return Response
     */
    public function about(): Response
    {
        return $this->render('static/about.html.twig');
    }

    /**
     * @Route("/contacts", methods="GET", name="contacts")
     * @return Response
     */
    public function contacts(): Response
    {
        return $this->render('static/contacts.html.twig');
    }
}