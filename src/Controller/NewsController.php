<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\News1Type;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news")
 */
class NewsController extends AbstractController
{
    /**
     * @Route("/", name="news_index", methods={"GET"})
     * @param NewsRepository $newsRepository
     * @return Response
     */
    public function index(NewsRepository $newsRepository): Response
    {
        return $this->render('news/index.html.twig', [
            'news' => $newsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{slug}", methods={"GET"}, name="news_show")
     * @param News $item
     * @param NewsRepository $newsRepository
     * @return Response
     */
    public function cardShow(News $item, NewsRepository $newsRepository): Response
    {
        return $this->render('news/item_show.html.twig',
            [
                'item' => $item,
                'similarArticles' => $newsRepository->getSimilarArticles($item)
            ]);
    }
}
