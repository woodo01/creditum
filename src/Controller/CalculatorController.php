<?php
/**
 * Created by IntelliJ IDEA.
 * User: ivansigaev
 * Date: 2019-06-17
 * Time: 21:09
 */

namespace App\Controller;

use App\Repository\AttributeValueRepository;
use App\Repository\CardRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CalculatorController extends AbstractController
{
    /**
     * @Route("/calculator", methods="GET", name="calculator_index")
     * @param Request $request
     * @param AttributeValueRepository $attributeValueRepository
     * @param CardRepository $cardRepository
     * @return Response
     */
    public function calc(Request $request, AttributeValueRepository $attributeValueRepository, CardRepository $cardRepository): Response
    {
        $calculation = [
            'cards' => [],
            'table' => [],
            'chart' => []
        ];

        if ($request->query->has('calc') &&
            $request->query->has('income') &&
            $request->query->has('time') &&
            $request->query->has('rate')) {
            $income = $request->query->get('income');
            $time = $request->query->get('time');
            $rate = $request->query->get('rate');
            $calculation = $attributeValueRepository->calc($income, $time, $rate);
        }

        $calculation['cards'] = $cardRepository->findBy(['type' => 2], [], 3);
        $calculation['filter'] = null;
        return $this->render('calc/index.html.twig', $calculation);
    }
}