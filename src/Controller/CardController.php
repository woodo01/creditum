<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\AttributeValue;
use App\Entity\Card;
use App\Entity\CardType;
use App\Entity\Filter;
use App\Entity\FilterValue;
use App\Entity\PromoText;
use App\Events;
use App\Repository\AttributeValueRepository;
use App\Repository\CardRepository;
use App\Repository\FilterRepository;
use App\Repository\PromoTextRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends AbstractController
{
    private function findCardType(array $cards): ?CardType
    {
        /** @var Card $card */
        return isset($cards[0]) ? $cards[0]->getType() : null;
    }

    /**
     * @Route("/credit", methods={"GET"}, name="card_credit")
     * @param Request $request
     * @return Response
     */
    public function credit(Request $request): Response
    {
        return $this->defaultCardController($request, Card::CREDIT_CARD);
    }

    /**
     * @Route("/debit", methods={"GET"}, name="card_debit")
     * @param Request $request
     * @return Response
     */
    public function debit(Request $request): Response
    {
        return $this->defaultCardController($request, Card::DEBIT_CARD);
    }

    /**
     * @Route("/installment", methods={"GET"}, name="card_installment")
     * @param Request $request
     * @return Response
     */
    public function installment(Request $request): Response
    {
        return $this->defaultCardController($request, Card::INSTALLMENT_CARD);
    }

    /**
     * @Route("/virtual", methods={"GET"}, name="card_virtual")
     * @param Request $request
     * @return Response
     */
    public function virtual(Request $request): Response
    {
        return $this->defaultCardController($request, Card::VIRTUAL_CARD);
    }

    protected function defaultCardController(Request $request, int $cardType): Response
    {
        $cardRepository = $this->getDoctrine()->getRepository(Card::class);
        $filterRepository = $this->getDoctrine()->getRepository(Filter::class);

        $filterValues = $request->query->get('filter', []);

        if ($promoPage = $this->redirectToPage($filterValues)) {
            return $this->redirectToRoute('promotext_index', ['slug' => $promoPage->getSlug()], 301);
        }

        switch ($cardType) {
            case Card::DEBIT_CARD:
                $cardTypeUrl = 'card_debit';
                break;
            case Card::CREDIT_CARD:
                $cardTypeUrl = 'card_credit';
                break;
            case Card::INSTALLMENT_CARD:
                $cardTypeUrl = 'card_installment';
                break;
            case Card::VIRTUAL_CARD:
                $cardTypeUrl = 'card_virtual';
                break;
            default:
                $cardTypeUrl = 'card_index';

        }

        $cards = $cardRepository->getCards($cardType, $filterValues);
        $filter = $filterRepository->getFilter($cardType);
        return $this->render('card/index.html.twig', [
            'cards' => $cards,
            'cardType' => $this->findCardType($cards),
            'filter' => $filter,
            'filterChecked' => $filterValues,
            'promoText' => null,
            'cardUrl' => $cardTypeUrl
        ]);
    }
    protected function redirectToPage(array $filterValues): ?PromoText
    {
        /** @var PromoTextRepository $promoTextRepository */
        $promoTextRepository = $this->getDoctrine()->getRepository(PromoText::class);
        return $promoTextRepository->findPromoPageByFilters($filterValues);
    }

    /**
     * @Route("/filter/{value}", methods={"GET"}, name="card_filter", defaults={"value": "Альфа-Банк"})
     * @param Request $request
     * @param AttributeValue $attributeValue
     * @param CardRepository $cardRepository
     * @param AttributeValueRepository $attributeValueRepository
     * @param PromoTextRepository $promoTextRepository
     * @return Response
     */
    public function cardFilter(Request $request, AttributeValue $attributeValue, CardRepository $cardRepository, AttributeValueRepository $attributeValueRepository, PromoTextRepository $promoTextRepository): Response
    {
        $filter = $attributeValueRepository->getFilter();
//        $cards = $cardRepository->filter($attributeValue->getValue());
        $cards = $cardRepository->filterByValues($request->query->get('values', ''));
        $promoText = $promoTextRepository->findBy(['tag' => mb_strtolower($attributeValue->getValue())]);
        return $this->render('card/_main.html.twig', [
            'cards' => $cards,
            'cardType' => null,
            'filter' => $filter,
            'promoText' => $promoText ? $promoText[0]->getContent() : null
        ]);
    }

    /**
     * @Route("/card/{slug}", methods={"GET"}, name="card_show")
     * @param Card $card
     * @param AttributeValueRepository $attributeValueRepository
     * @return Response
     */
    public function cardShow(Card $card, AttributeValueRepository $attributeValueRepository): Response
    {
        $attributeValues = $attributeValueRepository->findBy(['card' => $card]);
        $params = [];

        switch ($card->getType()->getId()) {
            case 1:
                $cardTypeUrl = 'card_debit';
                break;
            case 2:
                $cardTypeUrl = 'card_credit';
                break;
            case 3:
                $cardTypeUrl = 'card_installment';
                break;
            case 4:
                $cardTypeUrl = 'card_virtual';
                break;
            default:
                $cardTypeUrl = 'card_index';

        }

        if ($cardTypeUrl === 'card_debit') {
            foreach ($attributeValues as $attributeValue) {
                if (1 === $attributeValue->getAttribute()->getId()) {
                    $params['bank'] = $attributeValue->getValueFormatted();
                }
                if (5 === $attributeValue->getAttribute()->getId()) {
                    $params['time'] = $attributeValue->getValueFormatted();
                }
                if (9 === $attributeValue->getAttribute()->getId()) {
                    $params['type'] = $attributeValue->getValueFormatted();
                }
            }
        } else {
            foreach ($attributeValues as $attributeValue) {
                if (2 === $attributeValue->getAttribute()->getId()) {
                    $params['limit'] = $attributeValue->getValue();
                }
                if (3 === $attributeValue->getAttribute()->getId()) {
                    $params['percent'] = $attributeValue->getValue();
                }
                if (4 === $attributeValue->getAttribute()->getId()) {
                    $params['grace_period'] = $attributeValue->getValueFormatted();
                }
            }
        }

        return $this->render('card/card_show.html.twig',
            ['card' => $card, 'card_type_url' => $cardTypeUrl, 'params' => $params]);
    }

    /**
     * @Route("/search", methods={"GET"}, name="blog_search")
     * @param Request $request
     * @param CardRepository $posts
     * @return Response
     */
    public function search(Request $request, CardRepository $posts): Response
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('blog/search.html.twig');
        }

        $query = $request->query->get('q', '');
        $limit = $request->query->get('l', 10);
        $foundPosts = $posts->findBySearchQuery($query, $limit);

        $results = [];
        foreach ($foundPosts as $post) {
            $results[] = [
                'title' => htmlspecialchars($post->getTitle(), ENT_COMPAT | ENT_HTML5),
                'date' => $post->getPublishedAt()->format('M d, Y'),
                'author' => htmlspecialchars($post->getAuthor()->getFullName(), ENT_COMPAT | ENT_HTML5),
                'summary' => htmlspecialchars($post->getSummary(), ENT_COMPAT | ENT_HTML5),
                'url' => $this->generateUrl('blog_post', ['slug' => $post->getSlug()]),
            ];
        }

        return $this->json($results);
    }
}
