<?php

namespace App\Repository;

use App\Entity\AttributeValue;
use App\Entity\Card;
use App\Form\CardType;
use App\Utils\Calculator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AttributeValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttributeValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttributeValue[]    findAll()
 * @method AttributeValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttributeValueRepository extends ServiceEntityRepository
{
    private $calc;

    public function __construct(RegistryInterface $registry, Calculator $calc)
    {
        parent::__construct($registry, AttributeValue::class);
        $this->calc = $calc;
    }

    public function findByGrouped(Card $card)
    {
        $items = $this->findBy(['card' => $card], ['attribute' => 'ASC']);

        $result = [];

        foreach ($items as $item) {
            $result[$item->getAttribute()->getId()]['attribute'] = $item->getAttribute();
            $result[$item->getAttribute()->getId()]['value'][] = $item->getValue();
            $result[$item->getAttribute()->getId()]['valueFormatted'][] = $item->getValueFormatted();
        }

        return array_map(function (array $items) {
            $items['value'] = implode(', ', $items['value']);
            $items['valueFormatted'] = implode(', ', $items['valueFormatted']);
            return $items;
        }, $result);

    }

    public function getFilter(CardType $cardType = null)
    {
        $sql = 'select count(c.id) count, av.value, av.value_formatted, a.name, a.id, av.id v_id from attribute_value av
                  inner join attribute a on a.id=av.attribute_id
                  inner join card c on av.card_id = c.id
                group by av.value, a.id
                order by a.id';

        $queryResult = $this->getEntityManager()->getConnection()->executeQuery($sql)->fetchAll();

        $result = [];
        foreach ($queryResult as $r) {
            $result[$r['id']]['name'] = $r['name'];
            $result[$r['id']]['collection'][] = ['id' => $r['v_id'], 'count' => $r['count'], 'value' => $r['value'], 'value_formatted' => $r['value_formatted']];
        }

        return $result;
    }

    public function calc($creditSum, $time, $rate)
    {
        return $this->calc->calc($creditSum, $time, $rate);
    }
}
