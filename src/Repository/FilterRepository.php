<?php

namespace App\Repository;

use App\Entity\CardType;
use App\Entity\Filter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Filter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Filter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Filter[]    findAll()
 * @method Filter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Filter::class);
    }

    public function getFilter(int $cardType)
    {
        $sql = 'select filter.id as id, filter.name as name, fv.id as fv_id, fv.value as value, count(c.id) as count from filter
                  left join filter_value fv on filter.id = fv.filter_id
                  left join card_filter_value cfv on fv.id = cfv.filter_value_id
                  left join card c on cfv.card_id = c.id
                where filter.card_type_id = :cardType
                group by fv.id;';

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('value', 'value');
        $rsm->addScalarResult('fv_id', 'v_id');
        $rsm->addScalarResult('count', 'count');

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameter('cardType', $cardType);
        $queryResult = $query->getResult();

        $result = [];
        foreach ($queryResult as $r) {
            $result[$r['id']]['name'] = $r['name'];
            $result[$r['id']]['collection'][] =
                ['id' => $r['v_id'], 'count' => $r['count'], 'value' => $r['value'], 'value_formatted' => $r['value']];
        }

        return $result;
    }

    // /**
    //  * @return Filter[] Returns an array of Filter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Filter
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
