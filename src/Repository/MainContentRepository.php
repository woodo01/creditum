<?php

namespace App\Repository;

use App\Entity\MainContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MainContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method MainContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method MainContent[]    findAll()
 * @method MainContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MainContentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MainContent::class);
    }

    // /**
    //  * @return MainContent[] Returns an array of MainContent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MainContent
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
