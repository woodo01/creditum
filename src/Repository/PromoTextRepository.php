<?php

namespace App\Repository;

use App\Entity\PromoText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PromoText|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromoText|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromoText[]    findAll()
 * @method PromoText[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoTextRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PromoText::class);
    }

    public function findPromoPageByFilters(array $filterValues): ?PromoText
    {
        if (!$filterValues) {
            return null;
        }

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb ->select('pt')
            ->from('App:PromoText', 'pt');

        foreach ($filterValues as $key => $filterValue) {
           $qb->join('pt.filterValues', "fv_$key", 'WITH', $qb->expr()->eq("fv_$key.id", $filterValue));
        }

        $promos = $qb->getQuery()->getResult();

        $ids = [];

        foreach ($promos as $promo) {
            $ids[$promo->getId()] = $promo;
        }

        $sql = 'select count(fv.filter_value_id) as count, pt.id as id from promo_text as pt
                  left join promo_text_filter_value fv on pt.id = fv.promo_text_id
                where pt.id in (:ids)
                group by pt.id;';

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('count', 'count');

        $query = $this->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameter('ids', array_keys($ids));

        foreach ($query->getResult() as $value) {
            if ((int)$value['count'] === count($filterValues)) {
                return $ids[$value['id']];
            }
        }

        return null;
    }
    // /**
    //  * @return PromoText[] Returns an array of PromoText objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromoText
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
