<?php

namespace App\Repository;

use App\Entity\Card;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Card|null find($id, $lockMode = null, $lockVersion = null)
 * @method Card|null findOneBy(array $criteria, array $orderBy = null)
 * @method Card[]    findAll()
 * @method Card[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Card::class);
    }

    public function filter($attrValue)
    {
        $sql = 'SELECT c0_.id FROM card c0_
                INNER JOIN attribute_value a1_ on a1_.card_id = c0_.id WHERE a1_.value = "' . $attrValue . '"';

        $ids = $this->getEntityManager()->getConnection()->executeQuery($sql)->fetchAll();

        $result = [];
        foreach ($ids as $array) {
            $result[] = $array['id'];
        }
        return $this->findBy(['id' => $result]);
    }

    public function filterByValues(string $ids)
    {
        $sql = 'SELECT c0_.id FROM card c0_
                INNER JOIN attribute_value a1_ on a1_.card_id = c0_.id WHERE a1_.value in (' . $ids . ')';

        $ids = $this->getEntityManager()->getConnection()->executeQuery($sql)->fetchAll();

        $result = [];
        foreach ($ids as $array) {
            $result[] = $array['id'];
        }
        return $this->findBy(['id' => $result]);
    }

    public function countSlug($slug): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(card.id)');
        $qb->from('App:Card','card');
        $qb->where('card.slug = ?1')
            ->setParameter('1', $slug);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getCards(int $cardType, array $filterValues)
    {
        if ($filterValues) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb ->select('c')
                ->from('App:Card', 'c')
                ->join('c.filterValues', 'fv', 'WITH', $qb->expr()->in('fv.id', $filterValues))
                ;

            if ($cardType) {
                $qb->where('c.type = ?1')->setParameter(1, $cardType);
            }

            return $qb->getQuery()->getResult();
        }

        return $this->findBy(['type' => $cardType]);
    }

    // /**
    //  * @return Card[] Returns an array of Card objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Card
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
