import '../scss/frontend.scss';
//
// import '../src/libs/popper.min.js';
//
// import '../src/libs/bs/util.js';
// import '../src/libs/bs/dropdown.js';
// import '../src/libs/nouislider/nouislider.min.js';
// import '../src/libs/slick-1.8.1/slick/slick.min.js';
//
// import '../src/js/scripts/dropdowns.js';
// import '../src/js/scripts/range-sliders.js';
// import '../src/js/scripts/cards-carousel.js';
//
// import '../src/js/scripts/tags.js';
// import '../src/js/scripts/table-scrollbar.js';
// import '../src/js/scripts/mobile-menu.js';
// import '../src/js/scripts/script.js';
//
// import '../src/js/scripts/init.js';

import {Decimal} from 'decimal.js';
window.Decimal = Decimal;

window.numeral = require('numeral');

window.numeral.register('locale', 'ru', {
    delimiters: {
        thousands: ' ',
        decimal: '.'
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return '' + number;
    },
    currency: {
        symbol: ''
    }
});

window.numeral.locale('ru');
window.numeral.defaultFormat('0,0.00');

import {Chart} from 'chart.js';
window.Chart = Chart;

import 'bootstrap/js/src/carousel';
