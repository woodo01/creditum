// меню и фильтры

function mobileMenu() {
  var $blocks = $('#header .header-nav, #aside');

  $blocks.each(function() {
    var $block = $(this),
        $toggle = $('#'+$block.data('toggle-id')),
        isActive = false;

    $toggle.on('change', function() {
      isActive = $toggle.prop('checked'); 
      (isActive) ? showMobileMenu($block) : hideMobileMenu($block);
    });

    $(window).on('resize', function() {
      if (isActive && !isMobileLayout()) {
        isActive = false;
        $toggle.prop('checked', false);
        hideMobileMenu($block, false);
      }
    });

  });

  // у айфона рендер через жопу, если включен плавный скролл css. творится херня с выделением флажков при скролле > 0. можно выключить css, но у айфонов будет убогий скролл, и тогда у таких пользователей не получится оформить микрозайм, чтоб купить себе нормальную трубу)
  if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) fixFiltersIOS();
}


function showMobileMenu($block, anim) {
  if (typeof anim == 'undefined') anim = true;
  $('body').css('overflow-y', 'hidden');
  if (anim) {
    $block.css('display', 'block');
    setTimeout(function() { $block.addClass('active').attr('style', ''); }, 10);
  } else {
    $block.addClass('active');
  }
}

function hideMobileMenu($block, anim) {
  if (typeof anim == 'undefined') anim = true;
  $('body').css('overflow-y', 'auto');
  if (anim) {
    $block.css('display', 'block').removeClass('active');
    setTimeout(function() { $block.attr('style', ''); }, 400);
  } else {
    $block.removeClass('active');
  }
}

function fixFiltersIOS() {
  var $aside = $('#aside');
  $aside.on('click', '.filter-checkbox__content, .btn-reset-filters', function() {
    var scr = $aside.scrollTop();
    $aside.scrollTop(scr+1).scrollTop(scr-1).scrollTop(scr); // топ лайфхак, но сработал
  });
}