function initCardsCarousel (selector) {

  var $selector = $(selector);
  if (!$selector.length) return false;

  $selector.slick({
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    infinite: false,
    prevArrow: '<div class="slick-prev"><i class="icon i-arrow i-arrow--left"></i></div>',
    nextArrow: '<div class="slick-next"><i class="icon i-arrow i-arrow--right"></i></div>'
  });

}