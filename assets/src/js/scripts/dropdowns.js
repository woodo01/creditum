function buildSelect() {
  var $selects = $('select.js-select');
  if(!$selects.length) return false;

  var $wraps = $selects.parent('.custom-select');

  // собираем селект из нативного
  $selects.each(function() {
    var $this = $(this),
        isAddClass = $this.attr('data-class'),
        className = (isAddClass) ? isAddClass : '',
        $options = $this.children(),
        addHtml = '',

        $thisOption,
        thisValue,
        classSelected = '',
        classPlaceholder = '',
        customOptions = '',
        isSelected, isDisabled,
        isPlaceholder = false, // true, if first is disabled && selected
        toggleText = '';

    for (var i = 0; i < $options.length; i++) {
      $thisOption = $options.eq(i);
      thisValue = $thisOption.val();
      isSelected = $thisOption.prop('selected');
      isDisabled = $thisOption.prop('disabled');

      if (isSelected) {
        toggleText = $thisOption.html();
        classSelected = ' selected';
        // первый опшн с пропами disabled selected будет просто подсказкой
        if (i == 0 && isDisabled) {
          isPlaceholder = true;
          classPlaceholder = ' placeholder';
        }
      }

      // если не плейсхолдер, добавить в список
      if (!isPlaceholder) { 
        customOptions += '<a class="dropdown-item'+classSelected+'" href="#" data-value="'+thisValue+'">'+$thisOption.html()+'</a>';
      }

      classSelected = '';
      isPlaceholder = false;
    }

    var html = ''+
      '<div class="dropdown '+className+'">'+
        '<a href="#" class="dropdown-toggle'+classPlaceholder+'" data-toggle="dropdown" data-display="static">'+toggleText+'</a>'+
        '<div class="dropdown-menu">'+customOptions+addHtml+'</div>'+
        '<i class="icon i-arrow i-arrow--down"></i>'+
      '</div>';

    $this.addClass('hidden-control').after(html);
  });

  // клик по списку, значение также изменяется у нативного
  $wraps.on('click', '.dropdown-item', function(e) {
    e.preventDefault();

    var $this = $(this),
        $box = $this.closest('.dropdown'),
        $control = $box.siblings('select'),
        $toggle = $box.children('.dropdown-toggle'),
        val = $this.attr('data-value'),
        html = $this.html();

    $this.addClass('selected').siblings().removeClass('selected');
    $toggle.removeClass('placeholder').html(html);
    $control.val(val);
  });

  // всплывет при ресете
  $selects.on('change', function() {
    var $this = $(this),
        val = $this.val(),
        $dropdown = $this.siblings('.dropdown'),
        $selected = $dropdown.find('[data-value='+val+']');

    $selected.trigger('click');
  });

  $('form').on('reset', function() {
    // value не обновляется в момент триггера, скидываем вручную
    var $this = $(this),
        $thisSelects = $this.find('select'),
        $thisOptions = $thisSelects.find('option');
    $thisOptions.prop('selected', false);
    $thisSelects.trigger('change');
  });
}


// дроп в сортировке каталога
// он без нативного контрола, код выше не применяется
function setDropdownSort() {
  var $dropdown = $('.dropdown-sort');
  if (!$dropdown.length) return false;

  $dropdown.on('click', '.dropdown-item', function(e) {
    e.preventDefault();
    var $this = $(this);
    $this.addClass('selected').siblings().removeClass('selected');
  });
}