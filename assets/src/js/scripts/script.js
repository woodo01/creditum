$(document).ready(function() {

  // в ie эта хрень не работает, а edge недавно смог
  $('button[form]').on('click', function(e) {
    var $this = $(this),
        $form = $('#'+$this.attr('form'));
        
    e.preventDefault();
    $form.submit();
  });

});

// в каталоге карт сменить вид
function switchItemsView(elem, view) {
  var $elem = $(elem),
      isActive = $elem.hasClass('active');

  if (isActive) {
    return false;
  } else {
    $elem.addClass('active').siblings().removeClass('active');
  }

  var $listItems = $('#main .cards-list');

  // две вьюшки - без велосипеда 
  if ($listItems.hasClass('cards-list--list-view')) {
    $listItems.removeClass('cards-list--list-view');
  } else {
    $listItems.addClass('cards-list--list-view');
  }
}

// стрелка в комментариях
function toggleComments(elem) {
  var $elem = $(elem);
  $elem.toggleClass('active').closest('.comments__top').siblings('.comments__row, .comments__list').toggle();
}

// показать еще в боковой колонке
function showMoreFilters(elem) {
  var $elem = $(elem),
      $filters = $elem.siblings('.js-aside-hidden-filters');
  $elem.toggleClass('active');
  $filters.slideToggle(200);
}

// своеобразный способ проверки на мобильное отображение (кнопка меню)
function isMobileLayout() {
  if ($('#js-is-mobile-checker').css('display') == 'none') {
    return false;
  } else {
    return true;
  } 
}