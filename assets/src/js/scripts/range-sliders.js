// опции берутся с атрибутов инпута number

function initRangeSliders(selector) {
  var $sliders = $(selector);
  if (!$sliders.length) return false;
  
  $sliders.each(function() {
    var $slider = $(this),
        slider = $slider[0],
        $box = $slider.parent('.js-calc-box'),
        $input = $slider.siblings('.js-calc-control'),
        $placeholder = $slider.siblings('.js-calc-placeholder'),
        $placeholderValue = $placeholder.children('.js-calc-val'),
        isTimeSlider = $slider.hasClass('js-calc-slider-time'),
        $placeholderMonth = (isTimeSlider) ? $placeholder.children('.js-calc-unit') : '';

    noUiSlider.create(slider, {
      start: $input.val(),
      connect: [true, false],
      step: +$input.attr('step'),
      range: {
        'min': +$input.attr('min'),
        'max': +$input.attr('max')
      }
    });

    var val, valFormat;

    slider.noUiSlider.on('update', function( values, handle ) {
      val = parseInt(values[handle]),
      valFormat = String(val).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');

      $input.val(val).attr('value', val); // attr для html (?)
      $placeholderValue.text(valFormat);

      if (isTimeSlider) $placeholderMonth.text(formatMonth(val));
    });

    $input.on('change', function() {
      slider.noUiSlider.set($input.val());
    });

  });
}


function formatMonth(n) {
  var m = ['месяц', 'месяца', 'месяцев'];
  if (n == 1) {
    return m[0];
  } else if (n < 5) {
    return m[1];
  } else {
    return m[2];
  }
}