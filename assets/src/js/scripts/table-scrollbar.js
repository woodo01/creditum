// захотелось своих скроллбаров, вроде не багает

function tableScrollbar() {
  var $block = $('.js-scroll-block');
  if (!$block.length) return false;

  var transformProp = (function(props, list) {
    for (var i = 0; i < props.length; i++) {
      if (props[i] in list) break;
    }
    return props[i];
  })(['transform', 'webkitTransform'], document.body.style);

  var $wrap = $block.parent('.js-scroll-wrap'),
      $table = $block.children('.js-scroll-table'),
      $scrollbar = $('<div class="table-scrollbar"></div>'),
      $track = $('<div class="table-scrollbar-track"></div>'),
      $thumb = $('<div class="table-scrollbar-thumb"></div>');

  $track.append($thumb).appendTo($scrollbar);
  $wrap.append($scrollbar);

  var blockWidth = $block.outerWidth(), tableWidth = $table.outerWidth(),
      blockScrollMax = 0, blockScrollVal = 0,
      sbScrollMax = 0, sbScrollVal = 0;

  show();
  getMax();
  
  $block.on('scroll', function() {
    blockScrollVal = $block.scrollLeft();
    sbScrollVal = blockScrollVal/blockScrollMax * sbScrollMax;
    if (drag) return;
    // в ios могут быть и такие значения..
    if (sbScrollVal > sbScrollMax) sbScrollVal = sbScrollMax;
    if (sbScrollVal < 0) sbScrollVal = 0;
    $thumb.css(transformProp, 'translateX('+sbScrollVal+'px)');
  });

  var drag = false,
      dragStart = 0,
      dragDone = 0;

  $thumb.on('touchstart mousedown', function(e) {
    e.preventDefault();
    drag = true;
    $thumb.addClass('active');
    dragStart = (e.type == 'mousedown') ? e.pageX : e.changedTouches[0].pageX;
    dragDone = sbScrollVal;
  });

  $(document).on('touchmove mousemove', function(e) {
    if (drag) {
      var x1 = (e.type == 'mousemove') ? e.pageX : e.changedTouches[0].pageX,
          x2 = dragDone + x1 - dragStart,
          x3 = x2 * blockScrollMax / sbScrollMax;

      $block.scrollLeft(x3);
      if (x3 > blockScrollMax) x2 = sbScrollMax;
      if (x3 < 0) x2 = 0;
      $thumb.css(transformProp, 'translateX('+x2+'px)');
    }
  });

  $(document).on('touchend mouseup', function() {
    if (drag) {
      drag = false;
      $thumb.removeClass('active');
    }
  });

  $(window).on('resize', function() {
    getMax();
    $block.trigger('scroll');
    show();
  });

  function getMax() {
    blockWidth = $block.outerWidth();
    tableWidth = $table.outerWidth();
    blockScrollMax = tableWidth - blockWidth;
    sbScrollMax = $track.outerWidth() - $thumb.outerWidth();
  }

  function show() {
    if (tableWidth <= blockWidth) {
      $scrollbar.removeClass('table-scrollbar--visible');
    } else if (!$scrollbar.hasClass('table-scrollbar--visible')) {
      $scrollbar.addClass('table-scrollbar--visible');
    }
  }

}