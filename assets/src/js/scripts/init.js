$(document).ready(function() {

    buildSelect();
    setDropdownSort();
    initRangeSliders('.js-calc-slider');
    initCardsCarousel('.js-cards-carousel');
    initTagsHuegx();
    tableScrollbar();
    mobileMenu();

});