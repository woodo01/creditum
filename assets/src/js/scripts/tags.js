// названия получше не придумал, но отображает всю суть скрипта)

function initTagsHuegx() {
  var $list = $('ul.js-tags-list');
  if (!$list.length) return false;

  var $items = $list.children();
  runTagsHuegx($list, $items);

  // может быть тяжело, оптимизация
  var resizeTimer;
  $(window).on('resize', function() {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() { runTagsHuegx($list, $items); }, 250);
  });
}

function runTagsHuegx($list, $items) {
  var $thisItem,
      thisWidth = 0,
      listWidth = $list.outerWidth(),
      w = 0,
      line = 1;

  for (var i = 0; i < $items.length; i++) {
    $thisItem = $items.eq(i);
    $thisItem.removeClass('first odd even');
    thisWidth = $thisItem.outerWidth();
    w += thisWidth;

    if (w > listWidth) {
      line++;
      $thisItem.addClass('first');
      (line % 2 == 0) ? $thisItem.addClass('even') : $thisItem.addClass('odd');
      thisWidth = $thisItem.outerWidth();
      w = thisWidth;
    } else {
      (line % 2 == 0) ? $thisItem.addClass('even') : $thisItem.addClass('odd');
    } // end if
  } // end for
}